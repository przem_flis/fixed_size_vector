#include "fixed_size_vector.hpp"
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

int main() {
  constexpr auto size = 1 << 15;
  using vec = utils::fixed_size_vector<std::string, size>;
  // using vec = std::vector<std::string>;
  vec sut;

  // sut.reserve(size);

  int sum = 0;
  for (int i = 0; i < size; ++i) {
    sut.emplace_back(std::to_string(i));
    sum += i;
    sut[i % 3] = std::to_string(i);

    if (i > 10 && i % 3 == 0) {
      sut.erase(sut.begin() + sut.size() / (i % 7 + 2));
    }
  }
  std::cout << sut[size / 2] << std::endl;
  return 0;
}
